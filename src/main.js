import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
// import 'bootstrap'
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

// [ CSS ]
// fontawesome
import '@/assets/fonts/fontawesome/css/fontawesome-all.min.css'
// animation
import '@/assets/plugins/animation/css/animate.min.css'
// prism
import '@/assets/plugins/prism/css/prism.min.css'
// vendor css
import '@/assets/css/style.css'

// [ JS ]
// vendor
// import '@/assets/js/vendor-all.min.js'
// bootstrap
// import '@/assets/plugins/bootstrap/js/bootstrap.min.js'
// pcoded
//import '@/assets/js/pcoded.min.js'
// menu-setting
//import '@/assets/js/menu-setting.min.js'
// prism
//import '@/assets/plugins/prism/js/prism.min.js'

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
